provider "aws" {
  region = "us-east-1" 
  access_key = "AKIAZYOGQOIBQJOLOG4O"
  secret_key = "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"
}




# Create a VPC
resource "aws_vpc" "my_vpc-DMS" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "my-vpc-dms"
  }
}

# Create a subnet
resource "aws_subnet" "my_subnet1-dms" {
  vpc_id                  = aws_vpc.my_vpc-DMS.id
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = true
  tags = {
    Name = "my-subnet1"
  }
}
resource "aws_subnet" "my_subnet2-dms" {
  vpc_id                  = aws_vpc.my_vpc-DMS.id
  cidr_block              = "10.0.2.0/24"
  availability_zone       = "us-east-1b"
  map_public_ip_on_launch = true
  tags = {
    Name = "my-subnet2"
  }
}
resource "aws_subnet" "my_subnet3-dms" {
  vpc_id                  = aws_vpc.my_vpc-DMS.id
  cidr_block              = "10.0.3.0/24"
  availability_zone       = "us-east-1c"
  map_public_ip_on_launch = true
  tags = {
    Name = "my-subnet3"
  }
}


# Create a new replication subnet group
resource "aws_dms_replication_subnet_group" "example" {
  replication_subnet_group_description = "Example replication subnet group"
  replication_subnet_group_id          = "example-dms-replication-subnet-group-tf"

  subnet_ids = [
    aws_subnet.my_subnet1-dms,
    aws_subnet.my_subnet2-dms,
    aws_subnet.my_subnet3-dms
  ]

  tags = {
    Name = "example"
  }
}


#create endpoints
resource "aws_dms_endpoint" "source_endpoint" {
    endpoint_id                 = "test-dms-endpoint-source-tf"
#   endpoint_identifier = "source-endpoint"
  endpoint_type      = "source"
  engine_name        = "sqlserver"
  username           = "source_username"
  password           = "source_password"
  server_name        = "source-server.endpoint.com" # Replace with your SQL Server endpoint
  port               = 1433
  database_name      = "source_database"
  extra_connection_attributes = "integratedSecurity=true;"
}

resource "aws_dms_endpoint" "target_endpoint" {
    endpoint_id  = "test-dms-endpoint-target-tf"
#   endpoint_identifier = "target-endpoint"
  endpoint_type      = "target"
  engine_name        = "mysql"
  username           = "target_username"
  password           = "target_password"
  server_name        = "target-endpoint.rds.amazonaws.com" # Replace with your RDS MySQL endpoint
  port               = 3306
  database_name      = "target_database"
}

resource "aws_dms_replication_instance" "my_dms_instance" {
    replication_instance_id      = "test-dms-replication-instance-tf"
  replication_instance_class = "dms.r5.large"
  allocated_storage         = 100
}

# resource "aws_dms_replication_task" "my_dms_task" {
#     replication_task_id       = "test-dms-replication-task-tf"
#   migration_type                = "full-load"
#   replication_instance_arn      = aws_dms_replication_instance.test-dms-replication-instance-tf.replication_instance_arn
#   source_endpoint_arn           = aws_dms_endpoint.source_endpoint.arn
#   target_endpoint_arn           = aws_dms_endpoint.target_endpoint.arn
#   table_mappings                = jsonencode({
#     "rules" : [
#       {
#         "rule-type"         : "selection",
#         "rule-id"           : "1",
#         "rule-name"         : "1",
#         "object-locator"    : {
#           "schema-name"     : "%",
#           "table-name"      : "%"
#         },
#         "rule-action"       : "include"
#       }
#     ]
#   })

#   replication_task_settings = jsonencode({
#     "cdcStartTime"   : "2013-10-23T00:00:00",
#     "cdcStopTime"    : "2013-10-24T00:00:00",
#     "tableMappings"  : "JSON_MAPPING"
#   })
# }

# Create a new replication task
resource "aws_dms_replication_task" "test" {
  cdc_start_time            = 1484346880
  migration_type            = "full-load"
  replication_instance_arn  = aws_dms_replication_instance.test-dms-replication-instance-tf.replication_instance_arn
  replication_task_id       = "test-dms-replication-task-tf"
  replication_task_settings = "..."
  source_endpoint_arn       = aws_dms_endpoint.test-dms-source-endpoint-tf.endpoint_arn
  table_mappings            = "{\"rules\":[{\"rule-type\":\"selection\",\"rule-id\":\"1\",\"rule-name\":\"1\",\"object-locator\":{\"schema-name\":\"%\",\"table-name\":\"%\"},\"rule-action\":\"include\"}]}"

  tags = {
    Name = "test"
  }

  target_endpoint_arn = aws_dms_endpoint.test-dms-target-endpoint-tf.endpoint_arn
}



output "dms_endpoint_source_arn" {
  value = aws_dms_endpoint.source_endpoint.arn
}

output "dms_endpoint_target_arn" {
  value = aws_dms_endpoint.target_endpoint.arn
}

output "dms_replication_task_arn" {
  value = aws_dms_replication_task.my_dms_task.arn
}
